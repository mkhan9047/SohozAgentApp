package app.nazmul.sohoz.sohozapp.Model;

/**
 * Created by Mujahid on 8/24/2018.
 */

public class VerifyMail {
    private int success;
    private String message;

    public VerifyMail(int success, String message) {
        this.success = success;
        this.message = message;
    }

    public int getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
