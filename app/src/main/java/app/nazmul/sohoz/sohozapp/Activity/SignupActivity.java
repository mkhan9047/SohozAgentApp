package app.nazmul.sohoz.sohozapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import app.nazmul.sohoz.sohozapp.LocalStorage.Storage;
import app.nazmul.sohoz.sohozapp.Model.SignUpResult;
import app.nazmul.sohoz.sohozapp.Model.User;
import app.nazmul.sohoz.sohozapp.Networking.RetrofitClient;
import app.nazmul.sohoz.sohozapp.Networking.RetrofitInterface;
import app.nazmul.sohoz.sohozapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignupActivity extends AppCompatActivity {

    EditText name, email, password , confirmPassword, referId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        InitView();


    }




    public void signUp(String name, String password, int refer, String phone){
        final Storage storage = new Storage(this);
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Signing Up.....");
        dialog.show();
        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofit().create(RetrofitInterface.class);
       Call<SignUpResult> signUpResultCall = retrofitInterface.getSignUpResult(name,password, refer, phone);
       signUpResultCall.enqueue(new Callback<SignUpResult>() {
           @Override
           public void onResponse(Call<SignUpResult> call, Response<SignUpResult> response) {

               SignUpResult result = response.body();
               if(result != null){

                   if(result.getSuccess()==0){

                       if(result.getMessage().contains("Duplicate entry") && result.getMessage().contains("email_address")){

                           Toast.makeText(SignupActivity.this, "Email already used!", Toast.LENGTH_SHORT).show();
                       }



                   }else {

                       Toast.makeText(SignupActivity.this, "Sign Up Success", Toast.LENGTH_SHORT).show();
                       Intent intent = new Intent(SignupActivity.this, varificationAcitivity.class);
                       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                       startActivity(intent);
                       finish();
                       GetUserData();
                       storage.SaveLogInSate(true);
                   }
               }

               if(dialog.isShowing()){
                   dialog.dismiss();
               }
           }


           @Override
           public void onFailure(Call<SignUpResult> call, Throwable t) {

           }
       });

    }



    private String getText(EditText s){
        return  s.getText().toString();
    }


    private void GetUserData(){
        final Storage storage = new Storage(this);

        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofit().create(RetrofitInterface.class);
       Call<User> getUser =  retrofitInterface.getUserData(email.getText().toString());
       getUser.enqueue(new Callback<User>() {
           @Override
           public void onResponse(Call<User> call, Response<User> response) {
               User user = response.body();
               if(user != null){
                   storage.SaveCurrentUSer(user.getAgent_name());
                   storage.SaveEmail(user.getEmail());
                   storage.SaveOTP(user.getOtp_number());
               }

           }

           @Override
           public void onFailure(Call<User> call, Throwable t) {

           }
       });
    }

    private void InitView(){
        name = findViewById(R.id.signup_name);
        email = findViewById(R.id.signup_email);
        password = findViewById(R.id.signup_password);
        confirmPassword = findViewById(R.id.signup_confirmpassword);
        referId = findViewById(R.id.signup_referid);
    }

    public void onSignUp(View view) {

        if(getText(name).length() > 0 && getText(referId).length() > 0 && getText(email).length() > 0 ) {

            if(getText(password).equals(getText(confirmPassword))){

                if(getText(password).length() > 6 ){

                    signUp(getText(name), getText(password), Integer.parseInt(getText(referId)), getText(email));

                }else{
                    Toast.makeText(this, "password can't be less than 6 digit!", Toast.LENGTH_SHORT).show();
                }

            }else{

                Toast.makeText(this, "password not match", Toast.LENGTH_SHORT).show();
            }

        }else {

            Toast.makeText(this, "Please input every thing first ", Toast.LENGTH_SHORT).show();
        }


    }
}
