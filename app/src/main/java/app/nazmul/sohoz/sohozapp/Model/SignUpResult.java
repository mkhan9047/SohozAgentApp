package app.nazmul.sohoz.sohozapp.Model;

/**
 * Created by Mujahid on 8/17/2018.
 */

public class SignUpResult {

    private int success;
    private String message;

    public SignUpResult(int success, String message) {
        this.success = success;
        this.message = message;
    }

    public int getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }
}
