package app.nazmul.sohoz.sohozapp.Model;

/**
 * Created by Mujahid on 8/24/2018.
 */

public class User {
    private String agent_name;
    private String email;
    private int otp_number;

    public User(String agent_name, String email, int otp_number) {
        this.agent_name = agent_name;
        this.email = email;
        this.otp_number = otp_number;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public String getEmail() {
        return email;
    }

    public int getOtp_number() {
        return otp_number;
    }
}
