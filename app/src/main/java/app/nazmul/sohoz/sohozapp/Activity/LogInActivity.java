package app.nazmul.sohoz.sohozapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import app.nazmul.sohoz.sohozapp.LocalStorage.Storage;
import app.nazmul.sohoz.sohozapp.Model.SignIn;
import app.nazmul.sohoz.sohozapp.Networking.RetrofitClient;
import app.nazmul.sohoz.sohozapp.Networking.RetrofitInterface;
import app.nazmul.sohoz.sohozapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogInActivity extends AppCompatActivity {

    EditText email, password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        InitView();


    }

    private void InitView(){
        email = findViewById(R.id.sign_in_email);
        password = findViewById(R.id.sign_in_password);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Storage storage = new Storage(this);

        if(storage.getLogInState() && (!storage.getisVerified())){

            Intent intent = new Intent(LogInActivity.this, varificationAcitivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();


        }else if(storage.getLogInState() && storage.getisVerified()){
            Intent intent = new Intent(LogInActivity.this, Dashboard.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

    }

    public void onSignIn(View view) {
   getSignInResult();
    }

    public void onSignUp(View view) {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }

    private void getSignInResult(){
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Signing in....");
        dialog.show();
        final Storage storage = new Storage(this);
        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofit().create(RetrofitInterface.class);
       Call<SignIn> getSigninResult =  retrofitInterface.getSignInResult(email.getText().toString(), password.getText().toString());
       getSigninResult.enqueue(new Callback<SignIn>() {
           @Override
           public void onResponse(Call<SignIn> call, Response<SignIn> response) {

               SignIn signIn = response.body();
               if(signIn != null){
                   if(signIn.getSuccess() == 1){
                       storage.SaveLogInSate(true);
                       storage.SaveOTP(signIn.getOtp_number());
                       storage.SaveEmail(signIn.getEmail());
                       storage.SaveCurrentUSer(signIn.getAgent_name());

                       Intent intent = new Intent(LogInActivity.this, Dashboard.class);
                       intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                       startActivity(intent);
                       finish();
                   }else{
                       Toast.makeText(LogInActivity.this, "Invalid email or password!", Toast.LENGTH_SHORT).show();
                   }

                   if(dialog.isShowing()){
                       dialog.dismiss();
                   }
               }
           }

           @Override
           public void onFailure(Call<SignIn> call, Throwable t) {

           }
       });
    }

}
