package app.nazmul.sohoz.sohozapp.Model;

/**
 * Created by Mujahid on 8/24/2018.
 */

public class SignIn {
    private String agent_name;
    private String email;
    private int otp_number;
    private int success;

    public SignIn(String agent_name, String email, int otp_number, int success) {
        this.agent_name = agent_name;
        this.email = email;
        this.otp_number = otp_number;
        this.success = success;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public String getEmail() {
        return email;
    }

    public int getOtp_number() {
        return otp_number;
    }

    public int getSuccess() {
        return success;
    }
}
