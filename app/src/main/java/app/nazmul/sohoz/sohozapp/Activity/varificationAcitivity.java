package app.nazmul.sohoz.sohozapp.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import app.nazmul.sohoz.sohozapp.LocalStorage.Storage;
import app.nazmul.sohoz.sohozapp.Model.VerifyMail;
import app.nazmul.sohoz.sohozapp.Networking.RetrofitClient;
import app.nazmul.sohoz.sohozapp.Networking.RetrofitInterface;
import app.nazmul.sohoz.sohozapp.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class varificationAcitivity extends AppCompatActivity {

    EditText first, second, third, fourth, fifth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_varification_acitivity);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
        InitView();
        ChangeFocus();

    }



    private void ChangeFocus(){
        first.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                    if(editable.length()==1){
                        second.requestFocus();
                        if (second.requestFocus()) {
                            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                        }
                    }
            }
        });

        second.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==1){
                    third.requestFocus();
                    if (third.requestFocus()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });

        third.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length()==1){
                    fourth.requestFocus();
                    if (fourth.requestFocus()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });

        fourth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(editable.length() == 1){
                    fifth.requestFocus();
                    if (fifth.requestFocus()) {
                        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
                    }
                }
            }
        });
    }


    private void InitView(){
        first = findViewById(R.id.verify_first);
        second = findViewById(R.id.verify_second);
        third = findViewById(R.id.verify_third);
        fourth = findViewById(R.id.verify_fourth);
        fifth = findViewById(R.id.verify_fifth);
    }

    private void getVerified(int code, int otp){

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage("Getting Verified....");
        dialog.show();
        final Storage storage = new Storage(this);
        RetrofitInterface retrofitInterface = RetrofitClient.getRetrofit().create(RetrofitInterface.class);
        Call<VerifyMail> getResult = retrofitInterface.getEmailVerified(code, otp);
        getResult.enqueue(new Callback<VerifyMail>() {
            @Override
            public void onResponse(Call<VerifyMail> call, Response<VerifyMail> response) {

                VerifyMail mail = response.body();

                if(mail != null){
                    if(mail.getSuccess() == 1){

                        storage.SaveVarifiedState(true);
                        Toast.makeText(varificationAcitivity.this, "You are now Verified User!", Toast.LENGTH_SHORT).show();
                        Intent inten = new Intent(varificationAcitivity.this, Dashboard.class);
                        inten.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(inten);
                        finish();

                    }else if(mail.getSuccess()==0){
                        Toast.makeText(varificationAcitivity.this, "Invalid Verification Code!", Toast.LENGTH_SHORT).show();
                    }
                }

                if(dialog.isShowing()){
                    dialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<VerifyMail> call, Throwable t) {

            }
        });
    }

    public void onVerify(View view) {

        Storage storage = new Storage(this);

        EditText[] array = new EditText[5];

        array[0] = first;
        array[1] = second;
        array[2] = third;
        array[3] = fourth;
        array[4] = fifth;

        int code = Marger(array);

        getVerified(code, storage.getOTP());

    }

    private int Marger(EditText[] texts){

        StringBuilder builder = new StringBuilder();

        for(EditText data : texts){
            builder.append(data.getText().toString());
        }

        return Integer.parseInt(builder.toString());
    }
}
