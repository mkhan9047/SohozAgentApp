package app.nazmul.sohoz.sohozapp.Networking;

import app.nazmul.sohoz.sohozapp.Activity.SignupActivity;
import app.nazmul.sohoz.sohozapp.Model.SignIn;
import app.nazmul.sohoz.sohozapp.Model.SignUpResult;
import app.nazmul.sohoz.sohozapp.Model.User;
import app.nazmul.sohoz.sohozapp.Model.VerifyMail;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Mujahid on 8/17/2018.
 */

public interface RetrofitInterface {

    @GET("sohoz/agent/sign_up.php")
    Call<SignUpResult> getSignUpResult(@Query("name") String name, @Query("password") String password, @Query("ref_id") int ref_id, @Query("email") String email);


    @GET("sohoz/agent/verifiy_email.php")
    Call<VerifyMail> getEmailVerified(@Query("code")int verifyCode, @Query("otp") int otp);

    @GET("sohoz/agent/user_data.php")
    Call<User> getUserData(@Query("email") String email);

    @GET("sohoz/agent/sign_in.php")
    Call<SignIn> getSignInResult(@Query("email") String email, @Query("pass") String pass);


}
