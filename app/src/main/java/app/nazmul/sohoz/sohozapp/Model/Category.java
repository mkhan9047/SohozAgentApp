package app.nazmul.sohoz.sohozapp.Model;

import java.util.List;

/**
 * Created by Mujahid on 7/30/2018.
 */

public class Category {

    private List<CategoryName> catagory;

    public Category(List<CategoryName> catagory) {
        this.catagory = catagory;
    }

    public List<CategoryName> getCatagory() {
        return catagory;
    }

    public static class CategoryName {
        String name;
        int id;

        public CategoryName(String name, int id) {
            this.name = name;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public int getId() {
            return id;
        }
    }

}
