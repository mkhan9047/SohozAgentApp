package app.nazmul.sohoz.sohozapp.LocalStorage;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mujahid on 8/24/2018.
 */

public class Storage {
    //Constant Data

    private static final boolean LOGIN_SATE = false;
    private static final boolean isVerified = false;
    private static final String user_name = "N/A";
    private static final String email_address = "N/A";
    private static final int OTP = 0;




    private Context context;

    public Storage(Context context) {
        this.context = context;
    }


    private SharedPreferences.Editor getPreferencesEditor() {
        return getsharedPreferences().edit();
    }
    private SharedPreferences getsharedPreferences() {

        return context.getSharedPreferences("MyData", Context.MODE_PRIVATE);
    }


    public void SaveLogInSate(boolean p) {
        getPreferencesEditor().putBoolean("logged_in", p).commit();

    }

    public boolean getLogInState() {

        return getsharedPreferences().getBoolean("logged_in", LOGIN_SATE);
    }


    public void SaveVarifiedState(boolean p) {
        getPreferencesEditor().putBoolean("verify", p).commit();

    }

    public boolean getisVerified() {

        return getsharedPreferences().getBoolean("verify", isVerified);
    }


    public void SaveCurrentUSer(String name){
        getPreferencesEditor().putString("user",name).commit();
    }
    public String GetCurentUser(){
        return getsharedPreferences().getString("user",user_name);
    }

    public void SaveEmail(String email){
        getPreferencesEditor().putString("email",email).commit();
    }
    public String GetEmail(){
        return getsharedPreferences().getString("email",email_address);
    }

    public void SaveOTP(int otp){
        getPreferencesEditor().putInt("otp", otp).commit();
    }

    public int getOTP(){
        return getsharedPreferences().getInt("otp", OTP);
    }
}
